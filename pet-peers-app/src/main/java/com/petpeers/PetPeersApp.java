package com.petpeers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author 671003
 *
 */
@SpringBootApplication
public class PetPeersApp 
{
    public static void main( String[] args )
    {
    	SpringApplication.run(PetPeersApp.class, args);
    }
}
