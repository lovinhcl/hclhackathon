package com.petpeers.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PETS")
public class Pet extends BaseEntity{
	
	@Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
	
    @Column(name = "PET_NAME")
    private String petName;
    
    @Column(name = "PET_AGE")
    private Integer petAge;
    
    @Column(name = "PET_PLACE")
    private String petPlace;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PET_OWNERID")
    User user;
    
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public Integer getPetAge() {
		return petAge;
	}

	public void setPetAge(Integer petAge) {
		this.petAge = petAge;
	}

	public String getPetPlace() {
		return petPlace;
	}

	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}
    
//    @Column(name = "PET_OWNERID")
//    private User user;
    
}

    