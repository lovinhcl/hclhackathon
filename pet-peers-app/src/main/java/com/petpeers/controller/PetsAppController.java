package com.petpeers.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.petpeers.domain.Pet;
import com.petpeers.service.PetService;

@RestController
public class PetsAppController {
	private static final Logger logger = LogManager.getLogger(PetsAppController.class);

	@Autowired
	private PetService petService;

	@GetMapping("/pets")
	public List<Pet> getAllPets() {
		return petService.getAllPets();
	}

	@RequestMapping(value = "pets/addPet", method = RequestMethod.POST)
	public String addPets(@RequestBody Pet pets) {
		petService.addPets(pets);
		return "CREATED";
	}
}
