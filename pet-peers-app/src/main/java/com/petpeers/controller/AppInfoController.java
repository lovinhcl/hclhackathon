package com.petpeers.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 
 * @author 671003
 *
 */
//@Api(value = "news")
@RestController
public class AppInfoController {

	private static final Logger logger = LogManager.getLogger(AppInfoController.class);

	@GetMapping("/test")
	// @ApiOperation(value = "Get News", notes = "Returns news items")
	public String greeting() {
		logger.info("greeting");
		return "App is running..";
	}
}
