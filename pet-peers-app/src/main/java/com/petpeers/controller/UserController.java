package com.petpeers.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.petpeers.domain.Pet;
import com.petpeers.domain.User;
import com.petpeers.service.UserService;

@RestController
public class UserController {
	private static final Logger logger = LogManager.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@GetMapping("/users")
	public List<User> getAllPets() {
		return userService.getAllUser();
	}
	
	@RequestMapping(value = "users/addUser", method = RequestMethod.POST)
	public String addPets(@RequestBody User user) {
		userService.addUser(user);
		return "Done.";
	}

}
