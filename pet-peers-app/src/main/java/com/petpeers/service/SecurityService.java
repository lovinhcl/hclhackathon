package com.petpeers.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface SecurityService{
public ResponseEntity<String> authenticateUser();

}
