package com.petpeers.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.petpeers.domain.User;

@Service
public interface UserService {

	public List<User> getAllUser();

	public void addUser(User user);

}
