package com.petpeers.service;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.petpeers.domain.Pet;

public interface PetService {
	public ResponseEntity<String> savePet();

	/**
	 * This method is used to get all the pets from Database
	 * 
	 * @return List<Pets>
	 */
	public List<Pet> getAllPets();

	public void addPets(Pet pets);
}
