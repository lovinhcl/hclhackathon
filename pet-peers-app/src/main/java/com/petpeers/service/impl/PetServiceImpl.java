package com.petpeers.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.petpeers.dao.PetDao;
import com.petpeers.domain.Pet;
import com.petpeers.service.PetService;

@Service
public class PetServiceImpl implements PetService {
	// Logger log = Logger.getLogger(PetServiceImpl.class.getName());

	@Autowired
	PetDao petDao;

	@Override
	public ResponseEntity<String> savePet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pet> getAllPets() {
		return petDao.findAll();
	}

	@Override
	public void addPets(Pet pet) {
		//Optional<Pet> petDb = petDao.findById(pet.getId());
		petDao.save(pet);
	}

}
