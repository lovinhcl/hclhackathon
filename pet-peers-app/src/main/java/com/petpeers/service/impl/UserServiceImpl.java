package com.petpeers.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.petpeers.dao.UserRepo;
import com.petpeers.domain.Pet;
import com.petpeers.domain.User;
import com.petpeers.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	// Logger log = Logger.getLogger(PetServiceImpl.class.getName());

	@Autowired
	UserRepo userRepo;

	@Override
	public List<User> getAllUser() {
		return userRepo.findAll();
	}

	@Override
	public void addUser(User user) {
		userRepo.save(user);
	}

}
