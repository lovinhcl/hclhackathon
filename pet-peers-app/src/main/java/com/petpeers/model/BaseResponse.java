package com.petpeers.model;

/**
 * 
 * @author 671003
 *
 */
public class BaseResponse {

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
