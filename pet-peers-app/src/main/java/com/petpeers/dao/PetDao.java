package com.petpeers.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.petpeers.domain.Pet;

/**
 * 
 * @author 671003
 *
 */
@Repository
public interface PetDao extends JpaRepository<Pet, Long>{

}
