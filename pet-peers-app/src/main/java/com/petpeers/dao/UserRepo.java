package com.petpeers.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.petpeers.domain.User;

@Repository
public interface UserRepo extends JpaRepository<User, Long>{

}
